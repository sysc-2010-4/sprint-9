from sqlalchemy import create_engine, Column, Integer, String, Float, DateTime, ForeignKey
from sqlalchemy.orm import relationship, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from datetime import datetime

# Create the engine for SQLite
engine = create_engine('sqlite:///patient_monitoring.db', echo=True)

# Create a base class for declarative class definitions
Base = declarative_base()


# Define the Patient class
class Patient(Base):
    __tablename__ = 'patients'

    patient_id = Column(Integer, primary_key=True)
    username = Column(String, unique=True, nullable=False)
    password = Column(String, nullable=False)
    email = Column(String)
    age = Column(Integer)
    gender = Column(String)
    medical_history = Column(String)

    #Relationship between Patient and SensorData
    sensor_data = relationship("SensorData", back_populates="patient")


# Define the SensorData class
class SensorData(Base):
    __tablename__ = 'sensor_data'

    data_id = Column(Integer, primary_key=True)
    patient_id = Column(Integer, ForeignKey('patients.patient_id'))
    timestamp = Column(DateTime, default=datetime.now)
    heart_rate = Column(Float)
    blood_pressure_systolic = Column(Float)
    blood_pressure_diastolic = Column(Float)
    temperature = Column(Float)

    #Relationship between SensorData and Patient
    patient = relationship("Patient", back_populates="sensor_data")


# Create the tables in the database
Base.metadata.create_all(engine)

# Create a session
Session = sessionmaker(bind=engine)
session = Session()


# Function to register a new patient
def register_patient():
    print("New Patient Registration")
    username = input("Enter username: ")
    password = input("Enter password: ")
    email = input("Enter email: ")
    age = int(input("Enter age: "))
    gender = input("Enter gender: ")
    medical_history = input("Enter medical history: ")

    new_patient = Patient(username=username, password=password, email=email, age=age, gender=gender,
                          medical_history=medical_history)
    session.add(new_patient)
    session.commit()

    print("New patient registered successfully.")


# Function to add sensor data
def add_sensor_data():
    print("Adding Sensor Data")
    username = input("Enter patient's username: ")
    patient = session.query(Patient).filter_by(username=username).first()
    if not patient:
        print("Patient not found.")
        return

    heart_rate = float(input("Enter heart rate: "))
    blood_pressure_systolic = float(input("Enter systolic blood pressure: "))
    blood_pressure_diastolic = float(input("Enter diastolic blood pressure: "))
    temperature = float(input("Enter temperature: "))

    new_sensor_data = SensorData(patient=patient, heart_rate=heart_rate,
                                 blood_pressure_systolic=blood_pressure_systolic,
                                 blood_pressure_diastolic=blood_pressure_diastolic, temperature=temperature)
    session.add(new_sensor_data)
    session.commit()

    print("Sensor data added successfully.")


# Register a new patient
register_patient()

# Add sensor data
add_sensor_data()

# Close the session
session.close()





